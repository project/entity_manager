The Entity Manager module provide a speedy way to site builder for building
a uniform content authoring exprience for content author. It includes uniform
user interface and url path for any entity independent of the default drupal
admin page.

Features

Support most content entity type: node, taxonomy, block and custom type
Support most config entity type
Provide uniform url path for any entity type with /manage prefix: /manage/article, /manage/article/add, /manage/article/[id]/edit, /manage/article/[id]/delete ...
Provide interface corresponding permission structure
Plugable and configable entity operations and type action, you can add custom operations easily
Automatic manage list page, manage menu, page action ,page tab generated
Default admin theme and admin toolbar supported
Admin Toolbar module supported
Gin admin theme supported
Drupal 8/9/10 supported

Recommended modules

Switch Page Theme: specify a theme for content authoring path prefixed by /manage
Admin Toolbar: support mulitlevel menu
Gin admin theme: provide powerful authoring interface
