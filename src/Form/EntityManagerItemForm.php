<?php

namespace Drupal\entity_manager\Form;

use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_manager\EntityManagerPluginManager;
use Drupal\views\Entity\View;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity Manager Item form.
 *
 * @property \Drupal\entity_manager\EntityManagerItemInterface $entity
 */
class EntityManagerItemForm extends EntityForm implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type manager.
   *
   * @var EntityManagerPluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an EntityManagerPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityManagerPluginManager $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_manager'),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t('Label for the entity manager item.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\entity_manager\Entity\EntityManagerItem::load',
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
      '#description' => $this->t('Description of the entity manager item.'),
    ];

    $entity_types = $this->entityTypeManager->getDefinitions();
    $type_options = [];
    foreach($entity_types as $key => $item) {
      if($item instanceof ContentEntityType && !$item->hasHandlerClass('views_data')) continue;
      if($item instanceof ConfigEntityType && !$item->hasHandlerClass('list_builder')) continue;
      $type_options[$key] = $item->getLabel();
    }
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#options' => $type_options,
      '#default_value' => $this->entity->get('entity_type'),
      '#description' => $this->t('Entity Type of the entity manager item.'),
      '#required' => true,
      '#disabled' => !$this->entity->isNew(),
      '#ajax' => [
        'callback' => '::entityTypeCallback',
        'wrapper' => 'enity-bundle-warpper'
      ]
    ];
    $bundle_options = [];
    $form['entity_bundle'] = [
      '#prefix' => '<div id="enity-bundle-warpper">',
      '#suffix' => '</div>',
      '#type' => 'select',
      '#options' => $bundle_options,
      '#title' => $this->t('Entity Bundle'),
      '#default_value' => $this->entity->get('entity_bundle'),
      '#required' => true,
      '#disabled' => !$this->entity->isNew(),
      '#description' => $this->t('Entity Bundle of the entity manager item.'),
    ];
    $entity_type = $form_state->getValue('entity_type');
    if(empty($entity_type)) {
      $entity_type = $this->entity->get('entity_type');
    }
    if(!empty($entity_type)) {
      $form_state->setValue('entity_bundle', $entity_type);
      $entity_bundles = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type);
      foreach($entity_bundles as $key => $item) {
        $bundle_options[$key] = $item['label'];
      }
      $form['entity_bundle']['#options'] = $bundle_options;
    }

    $form['form_mode'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form Mode'),
      '#default_value' => $this->entity->get('form_mode')?:"default",
      '#description' => $this->t('Form Mode of the entity manager item.'),
    ];

    $plugins = $this->pluginManager->getDefinitions();
    uasort($plugins, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    $options = [];
    foreach($plugins as $plugin_id => $plugin) {
      $options[$plugin_id] = $plugin['label'];
    }
    $form['plugins'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Operations'),
      '#options' => $options,
      '#default_value' => $this->entity->isNew()?['add', 'edit', 'delete']:$this->entity->get('plugins'),
      '#description' => $this->t('Operation plugins of the entity manager item.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $message = $result == SAVED_NEW
    ? $this->t('Created new entity manager item %label.', $message_args)
    : $this->t('Updated entity manager item %label.', $message_args);
    $this->messenger()->addStatus($message);
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));

    // create manage views list:
    $entity_type = $this->entityTypeManager->getDefinition($this->entity->get('entity_type'));

    if($entity_type instanceof ContentEntityType) {
      if($entity_type->id() != 'taxonomy_term') {
        $this->createOrUpdateViews($entity_type);
      }
    }
    \Drupal::service('router.builder')->rebuild();
    return $result;
  }
  /**
   * create or update content entity views.
   *
   * @param ContentEntityType $entity_type
   * @return void
   */
  protected function createOrUpdateViews(ContentEntityType $entity_type) {
    $views_id = "manage_{$this->entity->id()}";
    $views = $this->entityTypeManager->getStorage('view')->load($views_id);
    if(empty($views)) {
      $values = [
        'id' => $views_id,
        'label' => "Manage {$this->entity->label()}",
        'module' => "views",
        'description' => "Manage {$this->entity->label()} list",
        'base_table' => $entity_type->getDataTable()?:$entity_type->getBaseTable(),
        'base_field' => $entity_type->getKey('id')
      ];

      $views = View::create($values);
      // default dispaly
      $views->addDisplay('default','Default', 'default');
      $default_display = &$views->getDisplay('default');
      $default_display['display_options']['title'] = "Manage {$this->entity->label()}";
      if($entity_type->hasKey('id')) {
        $default_display['display_options']['fields'][$entity_type->getKey('id')] = [
          "id" => $entity_type->getKey('id'),
          "table" => $entity_type->getDataTable(),
          "field" => $entity_type->getKey('id'),
          "relationship" => "none",
          "group_type" => "group",
          "admin_label" => "",
          "entity_type" => $entity_type->id(),
          "entity_field" => $entity_type->getKey('id'),
          "plugin_id" => "field",
          "label" => $entity_type->getKey('id'),
          "exclude" => false
        ];
      }
      if($entity_type->hasKey('label')) {
        $default_display['display_options']['fields'][$entity_type->getKey('label')] = [
          "id" => $entity_type->getKey('label'),
          "table" => $entity_type->getDataTable(),
          "field" => $entity_type->getKey('label'),
          "relationship" => "none",
          "group_type" => "group",
          "admin_label" => "",
          "entity_type" => $entity_type->id(),
          "entity_field" => $entity_type->getKey('label'),
          "plugin_id" => "field",
          "label" => $entity_type->getKey('label'),
          "exclude" => false
        ];
      }
      $default_display['display_options']['fields']["operations"] = [
        "id" => "operations",
        "table" => $entity_type->getBaseTable(),
        "field" => "operations",
        "relationship" => "none",
        "group_type" => "group",
        "admin_label" => "",
        "entity_type" => $entity_type->id(),
        "entity_field" => "entity_operations",
        "plugin_id" => "field",
        "label" => "Operations",
        "exclude" => false,
        "destination" => true
      ];

      $default_display['display_options']['pager'] = [
        "type" => "full",
        "options" => [
          "offset" => 0,
          "items_per_page" => 20
        ]
      ];
      $default_display['display_options']['access'] = [
        "type" => "perm",
        "options" => [
          "perm" => "manage entity {$this->entity->id()}",
        ]
      ];
      $default_display['display_options']['empty']['area'] = [
        "id" => "area",
        "table" => "views",
        "field" => "area",
        "relationship" => "none",
        "group_type" => "group",
        "admin_label" => "",
        "plugin_id" => "text",
        "empty" => true,
        "content" => [
          "value" => 'No records.',
          "format" => 'basic_html',
        ]
      ];
      $default_display['display_options']['style'] = [
        "type" => "table",
        "options" => [
          "empty_table" => true
        ]
      ];
      $default_display['display_options']['row'] = [
        "type" => "fields",
      ];

      // page list dispaly
      $views->addDisplay('page','Manage List', 'page_list');
      $default_display = &$views->getDisplay('page_list');
      $default_display['display_options']['path'] = "manage/{$this->entity->getPath()}";
    }

    // set views status
    $views->setStatus($this->entity->get('status'));
    $views->save();
  }
  /**
   * ajax callback
   *
   * @param array $form
   * @param FormStateInterface $form_state
   * @return void
   */
  public function entityTypeCallback(array &$form, FormStateInterface $form_state) {
    return $form['entity_bundle'];
  }

}
