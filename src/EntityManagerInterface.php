<?php

namespace Drupal\entity_manager;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_manager\Entity\EntityManagerItem;

/**
 * Interface for entity_manager plugins.
 */
interface EntityManagerInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

  public function getWeight();

  public function buildPage(EntityManagerItem $entity_manager_item, EntityInterface $entity = null);

  public function getPageTitle(EntityManagerItem $entity_manager_item, EntityInterface $entity = null);

  public function getPagePath(EntityManagerItem $entity_manager_item);

  public function getPageAccessRequirements(EntityManagerItem $entity_manager_item);

  public function getPermissions(EntityManagerItem $entity_manager_item);

  public function alterOperationLinks(EntityManagerItem $entity_manager_item, EntityInterface $entity, array &$operations);

  public function isMenuTask(EntityManagerItem $entity_manager_item);

  public function isMenuAction(EntityManagerItem $entity_manager_item);

}
