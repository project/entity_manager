<?php

namespace Drupal\entity_manager\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines entity_manager annotation object.
 *
 * @Annotation
 */
class EntityManager extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The plugin Weight.
   *
   * @var int
   */
  public $weight;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
