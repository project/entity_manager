<?php

namespace Drupal\entity_manager\Routing;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\Routing\Route;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_manager\EntityManagerPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Route subscriber.
 */
class EntityManagerRouting  implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type manager.
   *
   * @var EntityManagerPluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an EntityManagerPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityManagerPluginManager $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_manager'),
    );
  }

  /**
   * Route collection callback
   *
   * @return void
   */
  public function routes() {
    $entity_manager_items = $this->entityTypeManager->getStorage('entity_manager_item')->loadMultiple();
    $routes = [];
    foreach($entity_manager_items as $entity_manager_item) {
      if($entity_manager_item->get('status')) {
        $plugins = $this->pluginManager->getDefinitions();
        foreach($plugins as $plugin_id => $plugin) {
          if($this->pluginManager->isEnabled($entity_manager_item, $plugin_id)) {
            $routes["entity_manager.entity.{$entity_manager_item->id()}.{$plugin_id}_page"] = $this->getPluginRoute($entity_manager_item, $plugin_id);
          }
        }
        if($entity_manager_item->get('entity_type') == 'taxonomy_term') {
          $routes["entity_manager.entity.{$entity_manager_item->id()}.list_page"] = $this->getTaxonomyListRoute($entity_manager_item);
        }
        $entity_type = $this->entityTypeManager->getDefinition($entity_manager_item->get('entity_type'));
        if($entity_type instanceof ConfigEntityType) {
          $routes["entity_manager.entity.{$entity_manager_item->id()}.list_page"] = $this->getConfigEntityListRoute($entity_manager_item);
        }
      }
    }
    return $routes;
  }
  /**
   * taxonomy list route.
   *
   * @param EntityInterface $entity_manager_item
   * @return Route
   */
  protected function getTaxonomyListRoute(EntityInterface $entity_manager_item) {
    $route = $this->getRoute("/manage/{$entity_manager_item->getPath()}", $entity_manager_item, 'list');
    $route->addDefaults([
      "_controller" => '\Drupal\entity_manager\Controller\EntityManagerController::taxonomyListPage',
      "_title_callback" => '\Drupal\entity_manager\Controller\EntityManagerController::taxonomyListPageTitle',
    ]);
    $route->setRequirement("_permission", "manage entity {$entity_manager_item->id()}+bypass entity_manager entity access");
    return $route;
  }
  /**
   * config entity list route.
   *
   * @param EntityInterface $entity_manager_item
   * @return Route
   */
  protected function getConfigEntityListRoute(EntityInterface $entity_manager_item) {
    $route = $this->getRoute("/manage/{$entity_manager_item->getPath()}", $entity_manager_item, 'list');
    $route->addDefaults([
      "_controller" => '\Drupal\entity_manager\Controller\EntityManagerController::configEntityListPage',
      "_title_callback" => '\Drupal\entity_manager\Controller\EntityManagerController::configEntityListPageTitle',
    ]);
    $route->setRequirement("_permission", "manage entity {$entity_manager_item->id()}+bypass entity_manager entity access");
    return $route;
  }
  /**
   * entity plugin route.
   *
   * @param EntityInterface $entity_manager_item
   * @return Route
   */
  protected function getPluginRoute(EntityInterface $entity_manager_item, string $plugin_id) {
    $plugin_instance = $this->pluginManager->createInstance($plugin_id);
    $path = $plugin_instance->getPagePath($entity_manager_item);
    if($plugin_instance->isMenuAction($entity_manager_item)){
      $route = $this->getRoute("/manage/{$entity_manager_item->getPath()}/{$path}", $entity_manager_item, $plugin_id);
      $route->addDefaults([
        "_controller" => '\Drupal\entity_manager\Controller\EntityManagerController::actionPage',
        "_title_callback" => '\Drupal\entity_manager\Controller\EntityManagerController::actionPageTitle',
      ]);
    }else {
      $route = $this->getRoute("/manage/{$entity_manager_item->getPath()}/{entity}/{$path}", $entity_manager_item, $plugin_id);
      $route->addDefaults([
        "_controller" => '\Drupal\entity_manager\Controller\EntityManagerController::pluginPage',
        "_title_callback" => '\Drupal\entity_manager\Controller\EntityManagerController::pluginPageTitle',
      ]);
    }
    $requirements = $plugin_instance->getPageAccessRequirements($entity_manager_item);
    foreach($requirements as $key => $value) {
      $route->setRequirement($key, $value);
    }
    return $route;
  }
  /**
   * get configed base route object.
   *
   * @param String $path
   * @param EntityInterface $entity_manager_item
   * @param String $operation
   * @return Route
   */
  protected function getRoute(String $path, EntityInterface $entity_manager_item, String $operation) {
    $route = new Route($path);
    $route->setDefault('entity_manager_item', $entity_manager_item->id());
    $route->setDefault('operation', $operation);
    $route->setOption('parameters',[
      'entity_manager_item' => ['type' => 'entity:entity_manager_item'],
      'entity' => [
        'type' => 'entity:' . $entity_manager_item->get('entity_type'),
        'load_latest_revision' => true,
      ],
    ]);
    return $route;
  }

  /**
   * custom access
   *
   * @param AccountInterface $account
   * @param EntityInterface $entity_manager_item
   * @param String $operation
   * @return AccessResult
   */
  public function permissionAccess(AccountInterface $account, String $permission) {
    if($account->hasPermission("bypass entity_manager entity access")) {
      return AccessResult::allowed();
    }
    return AccessResult::allowedIfHasPermission($account, $permission);
  }
  /**
   * custom access
   *
   * @param AccountInterface $account
   * @param EntityInterface $entity_manager_item
   * @param EntityInterface $entity
   * @param String $operation
   * @return AccessResult
   */
  public function entityAccess(AccountInterface $account, EntityInterface $entity_manager_item, EntityInterface $entity, String $operation) {
    if($account->hasPermission("bypass entity_manager entity access")) {
      return AccessResult::allowed();
    }
    $owner_id = null;
    if($entity instanceof EntityOwnerInterface) {
      $owner_id = $entity->getOwnerId();
    }
    if($owner_id == $account->id() && $account->hasPermission("manage own entity {$entity_manager_item->id()} {$operation}")) {
      return AccessResult::allowed();
    }
    return AccessResult::allowedIfHasPermission($account, "manage any entity {$entity_manager_item->id()} {$operation}");
  }
}
