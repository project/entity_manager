<?php

namespace Drupal\entity_manager\Entity;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\entity_manager\EntityManagerItemInterface;

/**
 * Defines the entity manager item entity type.
 *
 * @ConfigEntityType(
 *   id = "entity_manager_item",
 *   label = @Translation("Entity Manager Item"),
 *   label_collection = @Translation("Entity Manager Items"),
 *   label_singular = @Translation("entity manager item"),
 *   label_plural = @Translation("entity manager items"),
 *   label_count = @PluralTranslation(
 *     singular = "@count entity manager item",
 *     plural = "@count entity manager items",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\entity_manager\EntityManagerItemListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_manager\Form\EntityManagerItemForm",
 *       "edit" = "Drupal\entity_manager\Form\EntityManagerItemForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "entity_manager_item",
 *   admin_permission = "administer entity_manager_item",
 *   links = {
 *     "collection" = "/admin/structure/entity-manager-item",
 *     "add-form" = "/admin/structure/entity-manager-item/add",
 *     "edit-form" = "/admin/structure/entity-manager-item/{entity_manager_item}",
 *     "delete-form" = "/admin/structure/entity-manager-item/{entity_manager_item}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "entity_type",
 *     "entity_bundle",
 *     "form_mode",
 *     "plugins"
 *   }
 * )
 */
class EntityManagerItem extends ConfigEntityBase implements EntityManagerItemInterface {

  /**
   * The entity manager item ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The entity manager item label.
   *
   * @var string
   */
  protected $label;

  /**
   * The entity manager item status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The entity_manager_item description.
   *
   * @var string
   */
  protected $description;
  /**
   * The entity_manager_item entity_type.
   *
   * @var string
   */
  protected $entity_type;
  /**
   * The entity_manager_item entity_bundle.
   *
   * @var string
   */
  protected $entity_bundle;
  /**
   * The entity_manager_item form_mode.
   *
   * @var string
   */
  protected $form_mode;
  /**
   * The entity_manager_item plugins.
   *
   * @var string
   */
  protected $plugins;

  public function getPath() {
    return Html::getId($this->id());
  }

}
