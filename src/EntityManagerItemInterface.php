<?php

namespace Drupal\entity_manager;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an entity manager item entity type.
 */
interface EntityManagerItemInterface extends ConfigEntityInterface {

}
