<?php

namespace Drupal\entity_manager\Menu;

use Drupal\Core\Menu\MenuActiveTrail;

/**
 * Class EntityManagerActiveTrail.
 *
 * @package Drupal\entity_manager\Menu
 */
class EntityManagerActiveTrail extends MenuActiveTrail {

  /**
   * {@inheritdoc}
   *
   * Change the active trail for node add/edit/view routes.
   */
  protected function doGetActiveTrailIds($menu_name) {
    $route = $this->routeMatch->getRouteObject();
    if($route->hasDefault('entity_manager_item')) {
      $entity_manager_item = $route->getDefault('entity_manager_item');
      $links = $this->getLinkByRoutes($menu_name, [
        ["entity_manager.entity.{$entity_manager_item}.list_page", []],
        ["view.manage_{$entity_manager_item}.page_list", []],
      ]);
    }
    if (!isset($links)) {
      return parent::doGetActiveTrailIds($menu_name);
    }

    $active_trail = ['' => ''];
    foreach($links as $link) {
      if ($parents = $this->menuLinkManager->getParentIds($link->getPluginId())) {
        $active_trail = $parents + $active_trail;
      }
    }

    return $active_trail;
  }

  /**
   * {@inheritdoc}
   *
   * The active trail logic is different here, so the active trails should be
   * cached separately.
   */
  protected function getCid() {
    if (!isset($this->cid)) {
      $this->cid = 'entity-manager-toolbar-' . parent::getCid();
    }

    return $this->cid;
  }

  /**
   * Get a possible link to base the active trail on.
   *
   * @param string $menu_name
   *   The name of the menu.
   * @param array $routes
   *   An array of route name & route params combinations in order of relevance.
   */
  protected function getLinkByRoutes(string $menu_name, array $routes) {
    foreach ($routes as $route) {
      [$route_name, $route_params] = $route;
      $links = $this->menuLinkManager->loadLinksByRoute($route_name, $route_params, $menu_name);
      return $links;
    }
    return NULL;
  }

}
