<?php

namespace Drupal\entity_manager\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_manager\EntityManagerPluginManager;
use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\taxonomy\Form\OverviewTerms;

/**
 * Returns responses for Entity Manager routes.
 */
class EntityManagerController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type manager.
   *
   * @var EntityManagerPluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an EntityManagerPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityManagerPluginManager $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_manager'),
    );
  }
  /**
   * taxonomy list page.
   */
  public function taxonomyListPage(EntityInterface $entity_manager_item, string $operation) {
    $vocabulary = Vocabulary::load($entity_manager_item->get('entity_bundle'));
    $form = $this->formBuilder()->getForm(OverviewTerms::class, $vocabulary);
    unset($form['terms']['#empty']);
    return $form;
  }
  /**
   * taxonomy list page title.
   */
  public function taxonomyListPageTitle(EntityInterface $entity_manager_item, string $operation) {
    return "Manage {$entity_manager_item->label()}";
  }
  /**
   * config entity list page.
   */
  public function configEntityListPage(EntityInterface $entity_manager_item, string $operation) {
    $entity_type = $this->entityTypeManager->getDefinition($entity_manager_item->get('entity_type'));
    if($entity_type->hasListBuilderClass()) {
      $list_builder = $this->entityTypeManager->getHandler($entity_manager_item->get('entity_type'),'list_builder');
      return $list_builder->render();
    }else {
      return [
        '#markup' => "No list builder class found."
      ];
    }
  }
  /**
   * config entity list page title.
   */
  public function configEntityListPageTitle(EntityInterface $entity_manager_item, string $operation) {
    return "Manage {$entity_manager_item->label()}";
  }
  /**
   * Entity action plugin page.
   */
  public function actionPage(EntityInterface $entity_manager_item, string $operation) {
    $instance = $this->pluginManager->createInstance($operation);
    $output = $instance->buildPage($entity_manager_item);
    return $output;
  }
  /**
   * Entity action plugin page title.
   */
  public function actionPageTitle(EntityInterface $entity_manager_item, string $operation) {
    $instance = $this->pluginManager->createInstance($operation);
    return $instance->getPageTitle($entity_manager_item);
  }
  /**
   * Entity plugin page.
   */
  public function pluginPage(EntityInterface $entity_manager_item, EntityInterface $entity, string $operation) {
    $instance = $this->pluginManager->createInstance($operation);
    $output = $instance->buildPage($entity_manager_item, $entity);
    if($entity) {
      $output['#cache'] = [
        'tags' => ["{$entity->getEntityTypeId()}:{$entity->id()}"]
      ];
    }
    return $output;
  }
  /**
   * Entity plugin page title.
   */
  public function pluginPageTitle(EntityInterface $entity_manager_item, EntityInterface $entity, string $operation) {
    $instance = $this->pluginManager->createInstance($operation);
    return $instance->getPageTitle($entity_manager_item, $entity);
  }

}
