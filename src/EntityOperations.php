<?php

namespace Drupal\entity_manager;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_manager\EntityManagerPluginManager;

/**
 * EntityOperations.
 */
class EntityOperations {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin.manager.entity_manager service.
   *
   * @var \Drupal\entity_manager\EntityManagerPluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an EntityOperations object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\entity_manager\pluginManagerPluginManager $entity_manager
   *   The plugin.manager.entity_manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityManagerPluginManager $entity_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $entity_manager;
  }

  /**
   * alter entity operations.
   */
  public function alterOperations(&$operations, EntityInterface $entity) {
    $entity_manager_items = $this->entityTypeManager->getStorage('entity_manager_item')->loadByProperties([
      'entity_type' => $entity->getEntityTypeId(),
      'entity_bundle' => $entity->bundle(),
      'status' => 1,
    ]);
    if(!empty($entity_manager_items)) {
      $entity_manager_item = reset($entity_manager_items);
      $plugins = $this->pluginManager->getDefinitions();
      foreach($plugins as $plugin_id => $plugin) {
        if($this->pluginManager->isEnabled($entity_manager_item, $plugin_id)) {
          $plugin_instance = $this->pluginManager->createInstance($plugin_id);
          $plugin_instance->alterOperationLinks($entity_manager_item, $entity, $operations);
        }else {
          unset($operations[$plugin_id]);
        }
      }
    }
  }
}
