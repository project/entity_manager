<?php

namespace Drupal\entity_manager\Plugin\EntityManager;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\entity_manager\Entity\EntityManagerItem;
use Drupal\entity_manager\EntityManagerPluginBase;

/**
 * Plugin implementation of the entity_manager.
 *
 * @EntityManager(
 *   id = "revisions",
 *   label = @Translation("Revision"),
 *   weight = 99,
 *   description = @Translation("Entity Revisions.")
 * )
 */
class Revisions extends EntityManagerPluginBase {

  public function buildPage(EntityManagerItem $entity_manager_item, EntityInterface $entity = null){
    if($entity instanceof RevisionableInterface) {
      $dateFormatter = \Drupal::service('date.formatter');
      $entity_storage = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId());

      $build['#title'] = t('Revisions for %title', ['%title' => $entity->label()]);
      $header = [t('Revision'), t('Operations')];

      $rows = [];
      $default_revision = $entity->getRevisionId();
      $current_revision_displayed = FALSE;

      foreach ($this->getRevisionIds($entity, $entity_storage) as $vid) {
        /** @var \Drupal\node\NodeInterface $revision */
        $revision = $entity_storage->loadRevision($vid);
        // Only show revisions that are affected by the language that is being
        // displayed.
        if ($revision->isRevisionTranslationAffected()) {
          $username = [
            '#theme' => 'username',
            '#account' => $revision->getRevisionUser(),
          ];

          // Use revision link to link to revisions that are not active.
          $date = $dateFormatter->format($revision->revision_timestamp->value, 'short');

          // We treat also the latest translation-affecting revision as current
          // revision, if it was the default revision, as its values for the
          // current language will be the same of the current default revision in
          // this case.
          $is_current_revision = $vid == $default_revision || (!$current_revision_displayed && $revision->wasDefaultRevision());
          if (!$is_current_revision && $entity->getEntityTypeId() == 'node') {
            $link = Link::fromTextAndUrl($date, new Url('entity.node.revision', ['node' => $entity->id(), 'node_revision' => $vid]))->toString();
          }
          else {
            $link = $entity->toLink($date)->toString();
            $current_revision_displayed = TRUE;
          }

          $row = [];
          $column = [
            'data' => [
              '#type' => 'inline_template',
              '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
              '#context' => [
                'date' => $link,
                'username' => \Drupal::service('renderer')->renderPlain($username),
                'message' => ['#markup' => $revision->revision_log->value, '#allowed_tags' => Xss::getHtmlTagList()],
              ],
            ],
          ];
          \Drupal::service('renderer')->addCacheableDependency($column['data'], $username);
          $row[] = $column;

          if ($is_current_revision) {
            $row[] = [
              'data' => [
                '#prefix' => '<em>',
                '#markup' => t('Current revision'),
                '#suffix' => '</em>',
              ],
            ];

            $rows[] = [
              'data' => $row,
              'class' => ['revision-current'],
            ];
          }
          else {
            $row[] = [
              'data' => [
                '#type' => 'operations',
                '#links' => $links,
              ],
            ];

            $rows[] = $row;
          }
        }
      }

      $build['node_revisions_table'] = [
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => $header,
        '#attached' => [
          'library' => ['node/drupal.node.admin'],
        ],
        '#attributes' => ['class' => 'node-revision-table'],
      ];

      $build['pager'] = ['#type' => 'pager'];

      return $build;
    }else {
      return [
        '#markup' => t('No revisions.')
      ];
    }
  }

  /**
   * Gets a list of revision IDs for a specific entity.
   *
   * @param EntityInterface $entity
   *   The entity.
   * @param RevisionableStorageInterface $entity_storage
   *   The storage handler.
   *
   * @return int[]
   *   Revision IDs (in descending order).
   */
  protected function getRevisionIds(EntityInterface $entity, RevisionableStorageInterface $entity_storage) {
    $result = $entity_storage->getQuery()
      ->accessCheck(TRUE)
      ->allRevisions()
      ->condition($entity->getEntityType()->getKey('id'), $entity->id())
      ->sort($entity->getEntityType()->getKey('revision'), 'DESC')
      ->pager(50)
      ->execute();
    return array_keys($result);
  }

}
