<?php

namespace Drupal\entity_manager\Plugin\EntityManager;

use Drupal\Core\Entity\EntityFormBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_manager\Entity\EntityManagerItem;
use Drupal\entity_manager\EntityManagerPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the entity_manager.
 *
 * @EntityManager(
 *   id = "add",
 *   label = @Translation("Add"),
 *   weight = 1,
 *   description = @Translation("Entity add action.")
 * )
 */
class EntityAdd extends EntityManagerPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type manager.
   *
   * @var EntityFormBuilder
   */
  protected $formBuilder;

  /**
   * Constructs an EntityManagerPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, EntityFormBuilder $form_builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
    );
  }

  public function buildPage(EntityManagerItem $entity_manager_item, EntityInterface $entity = null){
    $entity_type = $this->entityTypeManager->getDefinition($entity_manager_item->get('entity_type'));
    $storage = $this->entityTypeManager->getStorage($entity_manager_item->get('entity_type'));

    $form_key = 'add';
    $entity_form_class = $entity_type->getFormClass($form_key);
    if(empty($entity_form_class)) {
      $form_key = 'default';
    }
    $entity = $storage->create([
      $entity_type->getKey('bundle') => $entity_manager_item->get('entity_bundle'),
      $entity_type->getKey('uid') => \Drupal::currentUser()->id(),
    ]);
    $entity_form = $this->formBuilder->getForm($entity, $form_key);
    return $entity_form;
  }

  public function isMenuAction(EntityManagerItem $entity_manager_item){
    return true;
  }
}
