<?php

namespace Drupal\entity_manager\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_manager\EntityManagerPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu action for the entity.
 */
class EntityActionLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type manager.
   *
   * @var EntityManagerPluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an EntityManagerPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager, EntityManagerPluginManager $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $entity_manager_items = $this->entityTypeManager->getStorage('entity_manager_item')->loadMultiple();
    foreach ($entity_manager_items as $id => $entity_manager_item) {
      if($entity_manager_item->get('status')) {
        $plugins = $this->pluginManager->getDefinitions();
        foreach($plugins as $plugin_id => $plugin) {
          if($this->pluginManager->isEnabled($entity_manager_item, $plugin_id)) {
            $plugin_instance = $this->pluginManager->createInstance($plugin_id);
            if($plugin_instance->isMenuAction($entity_manager_item)){
              $links["entity_manager.entity.{$entity_manager_item->id()}.list_{$plugin_id}"] = [
                'id' => "entity_manager.entity.{$entity_manager_item->id()}.list_{$plugin_id}",
                'title' => "{$plugin_instance->label()} {$entity_manager_item->label()}",
                'route_name' => "entity_manager.entity.{$entity_manager_item->id()}.{$plugin_id}_page",
                'options' => ['query' => ['destination' => base_path() . 'manage/' . $entity_manager_item->getPath()]],
                'appears_on' => [
                  "entity_manager.entity.{$entity_manager_item->id()}.list_page",
                  "view.manage_{$entity_manager_item->id()}.page_list",
                ],
                "class" => '\Drupal\menu_ui\Plugin\Menu\LocalAction\MenuLinkAdd',
                "cache_contexts" => ['url.query_args']
              ];
            }
          }
        }
      }
    }

    return $links;
  }
}
