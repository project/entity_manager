<?php

namespace Drupal\entity_manager\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\Entity\ConfigEntityType;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Derivative class that provides the menu links for the entity.
 */
class EntityMenuLink extends DeriverBase implements ContainerDeriverInterface {

   /**
   * @var EntityTypeManagerInterface $entityTypeManager.
   */
  protected $entityTypeManager;

  /**
   * Creates a ProductMenuLink instance.
   *
   * @param $base_plugin_id
   * @param EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $entity_manager_items = $this->entityTypeManager->getStorage('entity_manager_item')->loadMultiple();
    foreach ($entity_manager_items as $id => $entity_manager_item) {
      if($entity_manager_item->get('status')) {
        $route_name = "view.manage_{$entity_manager_item->id()}.page_list";
        $entity_type = $this->entityTypeManager->getDefinition($entity_manager_item->get('entity_type'));
        if($entity_type instanceof ConfigEntityType || $entity_type->id() == 'taxonomy_term') {
          $route_name = "entity_manager.entity.{$entity_manager_item->id()}.list_page";
        }
        try {
          if(\Drupal::service("router.route_provider")->getRouteByName($route_name)) {
            $links[$id] = [
              'title' => $entity_manager_item->label(),
              'route_name' => $route_name,
              // 'parent' => "entity_manager.entity_link:{$parent_id}"
            ] + $base_plugin_definition;
          }
        } catch (RouteNotFoundException $th) {
        }
      }
    }
    return $links;
  }
}
