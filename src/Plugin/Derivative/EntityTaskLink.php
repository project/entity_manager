<?php

namespace Drupal\entity_manager\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entity_manager\EntityManagerPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides the menu task for the entity.
 */
class EntityTaskLink extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type manager.
   *
   * @var EntityManagerPluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an EntityManagerPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($base_plugin_id, EntityTypeManagerInterface $entity_type_manager, EntityManagerPluginManager $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $base_plugin_id,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $links = [];
    $entity_manager_items = $this->entityTypeManager->getStorage('entity_manager_item')->loadMultiple();
    foreach ($entity_manager_items as $id => $entity_manager_item) {
      if($entity_manager_item->get('status')) {
        $plugins = $this->pluginManager->getDefinitions();
        $base = false;
        foreach($plugins as $plugin_id => $plugin) {
          if($this->pluginManager->isEnabled($entity_manager_item, $plugin_id)) {
            $plugin_instance = $this->pluginManager->createInstance($plugin_id);
            if(!$plugin_instance->isMenuAction($entity_manager_item) && $plugin_instance->isMenuTask($entity_manager_item)){
              if(empty($base)) {
                $base = "entity_manager.entity.{$entity_manager_item->id()}.{$plugin_id}_page";
              }
              $links["entity_manager.entity.{$entity_manager_item->id()}.task_{$plugin_id}"] = [
                'id' => "entity_manager.entity.{$entity_manager_item->id()}.task_{$plugin_id}",
                'route_name' => "entity_manager.entity.{$entity_manager_item->id()}.{$plugin_id}_page",
                'title' => "{$plugin_instance->label()}",
                'base_route' => $base,
                'options' => ['query' => ['destination' => base_path() . 'manage/' . $entity_manager_item->getPath()]],
                "weight" => $plugin_instance->getWeight()
              ] + $base_plugin_definition;
            }
          }
        }
      }
    }
    return $links;
  }
}
