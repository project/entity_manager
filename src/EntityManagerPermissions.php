<?php

namespace Drupal\entity_manager;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * EntityManagerPermissions.
 */
class EntityManagerPermissions implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The entity type manager.
   *
   * @var EntityManagerPluginManager
   */
  protected $pluginManager;

  /**
   * Constructs an EntityManagerPermissions object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityManagerPluginManager $plugin_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->pluginManager = $plugin_manager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.entity_manager'),
    );
  }
  /**
   * Method description.
   */
  public function permissions() {
    $permissions = [];
    $entity_manager_items = $this->entityTypeManager->getStorage('entity_manager_item')->loadMultiple();
    $plugins = $this->pluginManager->getDefinitions();
    foreach($entity_manager_items as $entity_manager_item) {
      $permissions["manage entity {$entity_manager_item->id()}"] = [
        "title" => "{$entity_manager_item->label()}: Access list"
      ];

      foreach($plugins as $plugin_id => $plugin) {
        if($this->pluginManager->isEnabled($entity_manager_item, $plugin_id)) {
          $plugin_instance = $this->pluginManager->createInstance($plugin_id);
          $plugin_perms = $plugin_instance->getPermissions($entity_manager_item);
          $permissions = array_merge($permissions, $plugin_perms);
        }
      }
    }
    return $permissions;
  }

}
