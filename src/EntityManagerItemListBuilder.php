<?php

namespace Drupal\entity_manager;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Provides a listing of entity manager items.
 */
class EntityManagerItemListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['entity_type'] = $this->t('Entity Type');
    $header['entity_bundle'] = $this->t('Entity bundle');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\entity_manager\EntityManagerItemInterface $entity */
    $url = Url::fromUserInput("/manage/{$entity->getPath()}", ['attributes' => ['target' => '_blank']]);
    $row['label'] = Link::fromTextAndUrl($entity->label(), $url)->toString();
    $row['id'] = $entity->id();
    $row['entity_type'] = $entity->get('entity_type');
    $row['entity_bundle'] = $entity->get('entity_bundle');
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

}
