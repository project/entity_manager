<?php

namespace Drupal\entity_manager;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\entity_manager\Entity\EntityManagerItem;

/**
 * Base class for entity_manager plugins.
 */
abstract class EntityManagerPluginBase extends PluginBase implements EntityManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

  public function getWeight() {
    return (int) $this->pluginDefinition['weight'];
  }

  public function buildPage(EntityManagerItem $entity_manager_item, EntityInterface $entity = null){
    return [
      '#markup' => $this->label()
    ];
  }

  public function getPageTitle(EntityManagerItem $entity_manager_item, EntityInterface $entity = null){
    if($entity) {
      return "{$this->label()} : {$entity->label()}";
    }else {
      return "{$this->label()} {$entity_manager_item->label()}";
    }
  }

  public function getPagePath(EntityManagerItem $entity_manager_item){
    return Html::getId($this->getPluginId());
  }

  public function getPageAccessRequirements(EntityManagerItem $entity_manager_item){
    if($this->isMenuAction($entity_manager_item)) {
      return ["_permission" => "manage entity {$entity_manager_item->id()} {$this->getPluginId()}+bypass entity_manager entity access"];
    }else {
      return ["_custom_access" => '\Drupal\entity_manager\Routing\EntityManagerRouting::entityAccess'];
    }
  }

  public function getPermissions(EntityManagerItem $entity_manager_item) {
    if($this->isMenuAction($entity_manager_item)) {
      return [
        "manage entity {$entity_manager_item->id()} {$this->getPluginId()}" => [
          "title" => "{$entity_manager_item->label()}: Manage entity {$this->label()}"
        ]
      ];
    }else {
      return [
        "manage own entity {$entity_manager_item->id()} {$this->getPluginId()}" => [
          "title" => "{$entity_manager_item->label()}: Manage own entity {$this->label()}"
        ],
        "manage any entity {$entity_manager_item->id()} {$this->getPluginId()}" => [
          "title" => "{$entity_manager_item->label()}: Manage any entity {$this->label()}"
        ],
      ];
    }
  }

  public function alterOperationLinks(EntityManagerItem $entity_manager_item, EntityInterface $entity, array &$operations){
    if(!$this->isMenuAction($entity_manager_item)) {
      $url = Url::fromRoute("entity_manager.entity.{$entity_manager_item->id()}.{$this->getPluginId()}_page",
        [
          'entity' => $entity->id(),
          'destination' => \Drupal::destination()->get()
        ]
      );
      $access = \Drupal::accessManager()->checkNamedRoute($url->getRouteName(), $url->getRouteParameters());
      if($access) {
        $operations[$this->getPluginId()] = [
          'title' => $this->label(),
          'url' => $url,
          'weight' => $this->getWeight()
        ];
      }else {
        unset($operations[$this->getPluginId()]);
      }
    }
  }

  public function isMenuTask(EntityManagerItem $entity_manager_item){
    return TRUE;
  }

  public function isMenuAction(EntityManagerItem $entity_manager_item){
    return FALSE;
  }
}
